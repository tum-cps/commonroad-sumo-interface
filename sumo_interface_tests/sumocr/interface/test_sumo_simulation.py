"""
Module tests of the module scenario_generator
"""
import copy
import os
import sys
from typing import List
sys.path.append(os.path.join(os.path.dirname(__file__), "../../../"))

import numpy as np
from commonroad.common.file_reader import CommonRoadFileReader
from commonroad.common.file_writer import CommonRoadFileWriter, OverwriteExistingFile
from commonroad.planning.planning_problem import PlanningProblemSet
from commonroad.scenario.state import State, CustomState
from simulation.simulations import load_sumo_configuration, simulate_with_planner

from sumocr.interface.sumo_simulation import SumoSimulation
from sumocr.maps.sumo_scenario import ScenarioWrapper
from sumocr.sumo_config import DefaultConfig
from sumocr.visualization.video import create_video
from sumo_interface_tests.common.evaluation import deep_compare
from sumo_interface_tests.common.marker import *
from sumo_interface_tests.common.path import resource_root, output_root, reference_root

__author__ = "Peter Kocsis"
__copyright__ = "TUM Cyber-Physical System Group"
__credits__ = []
__version__ = "0.1"
__maintainer__ = "Peter Kocsis"
__email__ = "peter.kocsis@tum.de"
__status__ = "Integration"

resource_path = os.path.join(resource_root("test_sumo_simulation"))
reference_path = reference_root("test_sumo_simulation")
output_path = output_root("test_sumo_simulation")

@unit_test
@functional
def test_minimal_example():
    import copy
    import os
    from typing import List
    import inspect

    from commonroad.scenario.state import State
    from commonroad.common.file_reader import CommonRoadFileReader
    from commonroad.common.file_writer import CommonRoadFileWriter

    from sumocr.interface.sumo_simulation import SumoSimulation
    from sumocr.maps.sumo_scenario import ScenarioWrapper

    from example_scenarios.a9.scenario_config import Conf

    config = Conf()
    scenario_path = os.path.dirname(inspect.getfile(Conf))

    cr_file = os.path.abspath(
        os.path.join(scenario_path,
                     config.scenario_name + '.cr.xml'))

    # Change this as you see fit
    output_folder = os.path.dirname(cr_file)
    print("Reading file:", cr_file, " Outputing to folder:", output_folder)

    scenario, _ = CommonRoadFileReader(cr_file).open()
    wrapper = ScenarioWrapper.init_from_scenario(config, scenario_path, cr_map_file=cr_file)

    ##
    ## SIMULATION
    ##
    sumo_sim = SumoSimulation()
    sumo_sim.initialize(config, wrapper)

    for t in range(config.simulation_steps):
        ego_vehicles = sumo_sim.ego_vehicles
        commonroad_scenario = sumo_sim.commonroad_scenario_at_time_step(
            sumo_sim.current_time_step)

        # plan trajectories for all ego vehicles
        for id, ego_vehicle in ego_vehicles.items():
            current_state = ego_vehicle.current_state

            # plug in a trajectory planner here, currently staying on initial state
            next_state = copy.deepcopy(current_state)
            next_state.time_step = 1
            ego_trajectory: List[State] = [next_state]

            ego_vehicle.set_planned_trajectory(ego_trajectory)

        sumo_sim.simulate_step()

    sumo_sim.stop()

    ##
    ## WRITE TO FILE
    ##
    print("Done simulating")
    simulated_scenario = sumo_sim.commonroad_scenarios_all_time_steps()
    CommonRoadFileWriter(simulated_scenario,
                         None,
                         author=scenario.author,
                         affiliation=scenario.affiliation,
                         source=scenario.source,
                         tags=scenario.tags,
                         location=scenario.location).write_scenario_to_file(
        os.path.join(
            output_folder,
            config.scenario_name + ".simulated.xml"),
        overwrite_existing_file=True)


@pytest.mark.parametrize(
    ("time_step"),
    [0, 1, 2],
)
@unit_test
@functional
def test_commonroad_scenario_at_time_step(time_step):
    scenario_name = "DEU_A9-2_1_T-1"

    # Expected scenario
    ref_scenario_file_path = os.path.join(reference_path, f"{scenario_name}_step{time_step}.cr.xml")
    reference_scenario, _ = CommonRoadFileReader(ref_scenario_file_path).open()

    # Simulation
    scenario_file_path = os.path.join(resource_path, scenario_name, f"{scenario_name}.cr.xml")
    sumo_cfg_file = os.path.join(resource_path, scenario_name, f"{scenario_name}.sumo.cfg")
    scenario_wrapper = ScenarioWrapper()
    scenario_wrapper.initialize(scenario_name, sumo_cfg_file, scenario_file_path)

    config = DefaultConfig()
    config.scenario_name = scenario_name
    config.scenarios_path = resource_path

    sumo_sim = SumoSimulation()
    sumo_sim.initialize(config, scenario_wrapper)
    sumo_sim.dummy_ego_simulation = True
    for _ in range(time_step):
        sumo_sim.simulate_step()

    # Generated scenario
    out_scenario = sumo_sim.commonroad_scenario_at_time_step(time_step)
    sumo_sim.stop()

    out_scenario_file_path = os.path.join(output_path, f"{scenario_name}_step{time_step}.cr.xml")
    os.makedirs(os.path.dirname(out_scenario_file_path), exist_ok=True)
    CommonRoadFileWriter(out_scenario,
                         PlanningProblemSet(),
                         author="",
                         affiliation="",
                         source="",
                         tags={}).write_to_file(
        out_scenario_file_path,
        overwrite_existing_file=OverwriteExistingFile.ALWAYS)

    out_scenario, _ = CommonRoadFileReader(out_scenario_file_path).open()

    assert deep_compare(reference_scenario.dynamic_obstacles, out_scenario.dynamic_obstacles,
                        rtol=0.01, atol=0.001)

@pytest.mark.parametrize(
    ("all_time_step"),
    [100],
)
@unit_test
@functional
def test_commonroad_scenarios_all_time_steps(all_time_step):
    scenario_name = "DEU_A9-2_1_T-1"

    # Expected scenario
    ref_scenario_file_path = os.path.join(reference_path, f"{scenario_name}_step{all_time_step}.cr.xml")
    reference_scenario, _ = CommonRoadFileReader(ref_scenario_file_path).open()

    # Simulation
    scenario_file_path = os.path.join(resource_path, scenario_name, f"{scenario_name}.cr.xml")
    sumo_cfg_file = os.path.join(resource_path, scenario_name, f"{scenario_name}.sumo.cfg")
    scenario_wrapper = ScenarioWrapper()
    scenario_wrapper.initialize(scenario_name, sumo_cfg_file, scenario_file_path)

    config = DefaultConfig()
    config.scenario_name = scenario_name
    config.scenarios_path = resource_path

    sumo_sim = SumoSimulation()
    sumo_sim.initialize(config, scenario_wrapper)
    sumo_sim.dummy_ego_simulation = True
    for _ in range(all_time_step):
        sumo_sim.simulate_step()

    # Generated scenario
    out_scenario = sumo_sim.commonroad_scenarios_all_time_steps()
    sumo_sim.stop()

    out_scenario_file_path = os.path.join(output_path, f"{scenario_name}_step{all_time_step}.cr.xml")
    os.makedirs(os.path.dirname(out_scenario_file_path), exist_ok=True)
    CommonRoadFileWriter(out_scenario,
                         PlanningProblemSet(),
                         author="",
                         affiliation="",
                         source="",
                         tags={}).write_to_file(
        out_scenario_file_path,
        overwrite_existing_file=OverwriteExistingFile.ALWAYS)

    out_scenario, _ = CommonRoadFileReader(out_scenario_file_path).open()

    assert deep_compare(reference_scenario.dynamic_obstacles, out_scenario.dynamic_obstacles,
                        rtol=0.01, atol=0.001)

@pytest.mark.parametrize(
    ("initial_state", "expected_next_state"),
    [(CustomState(position=np.array([0,0]), orientation=0.2, velocity=5, acceleration=0, time_step=0),
     CustomState(position=np.array([0,0]), orientation=0.2, velocity=5, acceleration=0, time_step=1))],
)
@unit_test
@functional
def test_ego_simulate_step(initial_state, expected_next_state):
    scenario_name = "DEU_A9-2_1_T-1"

    # Simulation
    scenario_file_path = os.path.join(resource_path, scenario_name, f"{scenario_name}.cr.xml")
    sumo_cfg_file = os.path.join(resource_path, scenario_name, f"{scenario_name}.sumo.cfg")
    scenario_wrapper = ScenarioWrapper()
    scenario_wrapper.initialize(scenario_name, sumo_cfg_file, scenario_file_path)

    config = DefaultConfig()
    config.scenario_name = scenario_name
    config.scenarios_path = resource_path

    sumo_sim = SumoSimulation()
    sumo_sim.initialize(config, scenario_wrapper)
    assert len(sumo_sim.commonroad_scenario_at_time_step(sumo_sim.current_time_step).dynamic_obstacles) > 0
    assert len(sumo_sim.ego_vehicles) > 0
    while not sumo_sim.ego_vehicles:
        sumo_sim.simulate_step()

    ego_current = list(sumo_sim.ego_vehicles.values())[0]
    ego_trajectory = [expected_next_state]
    ego_current.set_planned_trajectory(ego_trajectory)

    sumo_sim.simulate_step()

    # Next state
    ego_next = list(sumo_sim.ego_vehicles.values())[0]
    out_next_state = ego_next.current_state
    sumo_sim.stop()

    np.testing.assert_equal(expected_next_state.position, out_next_state.position)
    np.testing.assert_equal(expected_next_state.orientation, out_next_state.orientation)
    np.testing.assert_equal(expected_next_state.velocity, out_next_state.velocity)


@pytest.mark.parametrize("scenario_name", ["DEU_Muc-7_1_I-1-1"])

@unit_test
@functional
def test_video(scenario_name):
    # Expected scenario
    scenario_path = os.path.join(os.path.dirname(__file__), f"../../resources/interactive_scenarios/{scenario_name}")
    conf = load_sumo_configuration(scenario_path)

    # Simulation
    # scenario_file_path = os.path.join(resource_path, scenario_name, f"{scenario_name}.cr.xml")
    out_path_video = os.path.join(os.path.dirname(__file__), f"../../outputs/test_video/")
    os.makedirs(os.path.dirname(out_path_video), exist_ok=True)

    scenario_wrapper = ScenarioWrapper.init_from_scenario(conf, scenario_path)
    sumo_sim = SumoSimulation()
    _, sumo_sim.planning_problem_set = CommonRoadFileReader(scenario_wrapper.cr_map_file).open()
    sumo_sim.initialize(conf, scenario_wrapper)

    for _ in range(min(50, conf.simulation_steps)):
        ego_vehicles = sumo_sim.ego_vehicles
        for id, ego_vehicle in ego_vehicles.items():
            current_state = ego_vehicle.current_state

            # plug in a trajectory planner here, currently staying on initial state
            next_state = copy.deepcopy(current_state)
            next_state.time_step = 1
            ego_trajectory: List[State] = [next_state]

            ego_vehicle.set_planned_trajectory(ego_trajectory)

        sumo_sim.simulate_step()

    # Generated scenario
    sumo_sim.stop()
    file = create_video(sumo_sim.commonroad_scenarios_all_time_steps(),
               out_path_video, trajectory_pred=sumo_sim.ego_vehicles, follow_ego=True)
    conf.plot_auto = True
    # os.path.isfile(file)
    # os.remove(file)