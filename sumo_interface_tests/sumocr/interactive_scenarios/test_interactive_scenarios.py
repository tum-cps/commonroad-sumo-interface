'''
Integration tests of the CommonRoad-SUMO-Manager repository
'''
import sys
import os
from copy import deepcopy

from commonroad.common.file_reader import CommonRoadFileReader
from sumocr.maps.sumo_scenario import ScenarioWrapper

sys.path.append(os.path.join(os.path.dirname(__file__), "../../../"))

from commonroad.common.solution import CommonRoadSolutionReader
from commonroad.scenario.scenario import Scenario
from simulation.simulations import simulate_without_ego, simulate_with_solution, simulate_scenario, SimulationOption, \
    load_sumo_configuration
from sumo_interface_tests.common.marker import *
from sumo_interface_tests.common.path import *
import multiprocessing
import pytest
import time


__author__ = "Peter Kocsis"
__copyright__ = "TUM Cyber-Physical System Group"
__credits__ = []
__version__ = "0.1"
__maintainer__ = "Peter Kocsis"
__email__ = "peter.kocsis@tum.de"
__status__ = "Integration"

USE_DOCKER_INTERFACE = False

@pytest.mark.parametrize("num_of_simulations", [1, 2, 8])
@integration_test
@functional
def test_simulate_scenario(num_of_simulations):
    scenario_name = "DEU_Muehlhausen-1_1_I-1-1"
    mp_pool = multiprocessing.Pool(num_of_simulations)
    interactive_scenario_path = os.path.join(os.path.dirname(__file__), "../../resources/interactive_scenarios", scenario_name)
    res = mp_pool.starmap(simulate_without_ego, [(interactive_scenario_path, None, False, USE_DOCKER_INTERFACE)
                                                 for _ in range(num_of_simulations)])
    print(res)
    # for process in processes:
    #     process.daemon = True
    #     process.start()
    #
    # for process in processes:
    #     process.join()

    # assert any([process.exitcode == 0 for process in processes])
@pytest.mark.parametrize("scenario_name", ["DEU_Frankfurt-98_2_I-1","DEU_Ibbenbueren-4_1_I-1-1", "DEU_Muc-7_1_I-1-1"])
@integration_test
@nonfunctional
def test_simulate_scenario_time(scenario_name):
    interactive_scenario_path = os.path.join(os.path.dirname(__file__), f"../../resources/interactive_scenarios/{scenario_name}")
    start = time.time()
    simulated_scenario_without_ego, planning_problem_set = simulate_without_ego(interactive_scenario_path,
                                                                                output_folder_path=None,
                                                                                use_sumo_manager = USE_DOCKER_INTERFACE)

    end = time.time()
    elapsed_time_sec = end - start
    assert elapsed_time_sec < 30.0


# @pytest.mark.timeout(60)
@pytest.mark.parametrize(
    "scenario_rel_path", [
        "DEU_Frankfurt-4_3_I-1",
    ],
)
@module_test
@functional
def test_compatibility_interactive_scenarios(scenario_rel_path):
    scenario_path = os.path.join(os.path.dirname(__file__), "../../resources/interactive_scenarios",
                                             scenario_rel_path)
    # scenario_path = os.path.join(resource_path_interactive_scenarios, scenario_rel_path)
    scenario_output_folder = os.path.join(output_path, os.path.dirname(scenario_rel_path))
    path_solutions = os.path.join(scenario_path, "solution.xml")
    solution = CommonRoadSolutionReader.open(path_solutions)

    # artificially extend solution to fit simulation steps
    last_state = solution.planning_problem_solutions[0].trajectory.state_list[-1]
    new_states = []
    for t in range(last_state.time_step + 1, 201):
        new_states.append(deepcopy(last_state))
        new_states[-1].time_step = t
    solution.planning_problem_solutions[0].trajectory.state_list.extend(new_states)

    scenario, pp, egos = simulate_with_solution(interactive_scenario_path=scenario_path,
                                                solution=solution,
                                                create_video=False,
                                                use_sumo_manager=False)
    assert isinstance(scenario, Scenario)


# @integration_test
# @functional
# def test_convert_scenario():
#     scenario_path = os.path.join(os.path.dirname(__file__), "resources", "a9.xml")
#     ref_path = reference_root("test_convert_scenario")
#     out_path = output_root("test_convert_scenario")
#     convert_scenario(scenario_path,
#                      config_type=CONFIG_TYPE.SUMO_CONFIG_1,
#                      output_folder_path=out_path)
#
#     def recursive_dir_compare(dcmp):
#         # TODO: set the seed of SUMO to ensure that always the same traffic is generated with the same settings
#         # assert len(dcmp.diff_files) == 0, f"Different file was found {dcmp.diff_files}"
#         assert len(dcmp.left_only) == 0, f"Missing file was found {dcmp.left_only}"
#         assert len(dcmp.right_only) == 0, f"Additional unnecessary file was found {dcmp.right_only}"
#         for sub_dcmp in dcmp.subdirs.values():
#             recursive_dir_compare(sub_dcmp)
#
#     dcmp = filecmp.dircmp(os.path.join(ref_path, scenario_path), os.path.join(out_path, scenario_path))
#     recursive_dir_compare(dcmp)


# @integration_test
# @nonfunctional
# def test_convert_scenario_time():
#     scenario_name = "a9.xml"
#     out_path = output_root("test_convert_scenario_time")
#     start = time.time()
#     convert_scenario(os.path.join(os.path.dirname(__file__), "resources/", scenario_name), config_type=CONFIG_TYPE.SUMO_CONFIG_1,
#                      output_folder_path=out_path)
#     end = time.time()
#     elapsed_time_sec = end - start
#     assert elapsed_time_sec < 60


@integration_test
@nonfunctional
def test_simulate_complex_scenario():
    scenario_name = "DEU_Muehlhausen-1_1_I-1-1"
    start = time.time()
    simulate_without_ego(os.path.join(os.path.dirname(__file__), "../../resources/interactive_scenarios", scenario_name),
                         create_video=False,
                         use_sumo_manager=USE_DOCKER_INTERFACE)
    

