# SUMO - CommonRoad Interface

This package implements the interface between the framework for motion planning of automated vehicles [CommonRoad_io](https://commonroad.in.tum.de/commonroad-io)
and the traffic simulator [SUMO](https://sumo.dlr.de).
The interface is presented in detail in our [paper](https://mediatum.ub.tum.de/doc/1486856/344641.pdf) [1].

## Documentation
Please refer to [commonroad.in.tum.de/sumo-interface](https://commonroad.in.tum.de/sumo-interface) for the documentation and tutorials.
To run interactive scenarios, denoted by the suffix ```I```, from the CommonRoad database,
please use the script from the corresponding repository [gitlab.lrz.de/tum-cps/commonroad-interactive-scenarios](https://gitlab.lrz.de/tum-cps/commonroad-interactive-scenarios).


[1] _Moritz Klischat, Octav Dragoi, Mostafa Eissa, and Matthias Althoff, Coupling SUMO with a Motion Planning Framework for Automated Vehicles, SUMO 2019: Simulating Connected Urban Mobility_
