# CommonRoad-SUMO-Manager Testing

This module contains the test system of the repository [CommonRoad-SUMO-Manager](https://gitlab.lrz.de/cps/commonroad-sumo-manager). It uses [pytest](https://docs.pytest.org/en/stable/) for test management with customized markers. 

## Test structure

```
└── tests
    ├── common
    |   └── marker.py                   -> Test marking helper methods
    ├── external
    |   └── test_<external_module_1>.py -> Contains tests relevant to <external_module_1>
    ├── resources                       -> Resources for tests
    |   └── <test_file>                 
    ├── references                      -> Contains reference files for the tests
    |   └── <test_name>     
    |       └── <reference_file>
    ├── outputs                         -> Contains the outputs of the last test run
    |   └── <test_name>     
    |       └── <output_file>
    ├── <module_1>                      -> Contains tests relevant to <module_1>
    |   ├── <submodule_1_1>                  -> Contains unittests of <module_1>.<submodule_1_1>
    |   |   └── test_<script>.py        -> Contains UNIT tests of <script>
    |   └── test_<module_1>.py          -> Contains MODULE tests of <module_1>
    ├── <module_2>
    :
    ├── <module_N>
    ├── test_commonroad_rl              -> Contains INTEGRATION tests
    ├── conftest.py                     -> Test configuration script
    ├── pytest.ini                      -> Test configuration settings
    └── README.md                       -> This file
```

## Test configuration
For the better test organization attributes can be defined using [markers](https://docs.pytest.org/en/latest/example/markers.html). Using these attributes the tests can be filtered. 
To make the indent clean and help to organize the tests, two mandatory markers has been defined for the tests:
* RunScope
  * unit
  * module
  * integration
* RunType
  * functional
  * non_functional
  
The definition of this types can be found under [common/marker.py](/testsmon/marker.py). **These two markers needs to be defined for all tests.**
The usage of custom markers has been restricted to make it easier to see what kind of markers are in use. Further markers:
* slow
* serial

If further markers are needed, define it in the [pytest.ini](/testsest.ini) file and add a wrapper in [common/marker.py](/testsmon/marker.py).

### Advanced configuration
If you need advanced test configuration you can use the [conftest.py](/testsftest.py) and define [hooks](https://docs.pytest.org/en/latest/writing_plugins.html#writing-hook-functions). 

## Write test
The pytest framework discovers all methods in the form `test_<method>`. Therefore you have to define your test function with a preceding `test_` string. 

**For all tests you have to define the two mandatory markers**. For better maintainability, please use the wrappers in the [common/marker.py](/testsmon/marker.py). 
```python
@pytest.mark.parametrize("num_of_simulations", [1, 2, 8])
@integration_test
@functional
def test_simulate_scenario(num_of_simulations):
    scenario_name = "a9"
    processes = list()
    for _ in range(num_of_simulations):
        processes.append(Process(target=simulate_scenario, args=(scenario_name,),
                                 kwargs={"scenario_folder": os.path.join(os.path.dirname(__file__), "resources")}))

    for process in processes:
        process.start()

    for process in processes:
        process.join()

    assert any([process.exitcode == 0 for process in processes])
```
You can use other features of pytest as well. 

## Run test
You have to call the test from the root of the repository
### Run a single test
You can run a specific test as follows
```bash
pytest <module>/[unittests]/test_<script>.py::test_<method>
```

### Run a batch of tests

For the test runs you can define the two main filters using
* --scope <scope_1> <scope_2> ...
* --type <type_1> <type_2> ...


Example:
```bash
pytest --scope unit module --type functional -m "slow"
```

By default the tests are not filtered, all tests will be run. 

For more and detailed options, please refer to [pytest](https://docs.pytest.org/en/stable/usage.html)