from sumo_interface_tests.common.marker import *

from sumocr.sumo_config import DefaultConfig
from sumocr.sumo_docker.rpc.sumo_client import SumoRPCClient
from commonroad.scenario.scenario import Scenario, ScenarioID
# from sumocr.sumo_docker import SumoCommonRoadConfig

import pytest

__author__ = "Peter Kocsis"
__copyright__ = "TUM Cyber-Physical Systems Group"
__credits__ = [""]
__version__ = "0.1.0"
__maintainer__ = "Peter Kocsis"
__email__ = "commonroad@lists.lrz.de"
__status__ = "Integration"


@pytest.mark.parametrize("obj_to_pack",
                         [None,
                          SumoRPCClient.RequestProvider._EMPTY_MESSAGE,
                          "Message",
                          DefaultConfig()])
@unit_test
@functional
def test_rpc_pack_unpack(obj_to_pack: object):
    def deep_comp(o1: object, o2: object) -> bool:
        if o1 is None:
            return o1 == o2

        o1d = getattr(o1, '__dict__', None)
        o2d = getattr(o2, '__dict__', None)

        # if both are objects
        if o1d is not None and o2d is not None:
            # we will compare their dictionaries
            o1, o2 = o1.__dict__, o2.__dict__

        if o1 is not None and o2 is not None:
            # if both are dictionaries, we will compare each key
            if isinstance(o1, dict) and isinstance(o2, dict):
                for k in set().union(o1.keys(), o2.keys()):
                    if k in o1 and k in o2:
                        if not deep_comp(o1[k], o2[k]):
                            return False
                    else:
                        return False  # some key missing
                return True
        return o1 == o2

    result = SumoRPCClient.RequestProvider._unpack(SumoRPCClient.RequestProvider._pack(obj_to_pack))
    assert deep_comp(result, obj_to_pack)

