"""
Helper module for single_Ibbenbueren evaluation
"""
import pytest
import inspect
from contextlib import contextmanager
import numpy as np

__author__ = "Peter Kocsis"
__copyright__ = "TUM Cyber-Physical System Group"
__credits__ = []
__version__ = "0.1"
__maintainer__ = "Peter Kocsis"
__email__ = "peter.kocsis@tum.de"
__status__ = "Integration"


@contextmanager
def does_not_raise():
    """
    Function which can be used for expecting that no exception will be raised
    Usage:
    @pytest.mark.parametrize("expected_exception", does_not_raise())
    def some_function(expected_exception):
        with expected_exception:
            do_the_test()
    """
    yield


def deep_compare(o1: object, o2: object, rtol: float = 0.0, atol: float = 0.0) -> bool:
    """
    Comapres all attributes of two objects
    :param o1: The first object
    :param o2: The second object
    :param rtol: Relative tolerance
    :param atol: Absolute tolerance
    :return: True if all attributes of the objects match
    """
    if o1 is None:
        return o1 == o2

    o1d = getattr(o1, "__dict__", None)
    o2d = getattr(o2, "__dict__", None)

    # if both are objects
    if o1d is not None and o2d is not None:
        # we will compare their dictionaries
        o1, o2 = o1.__dict__, o2.__dict__
    else:
        o1_vars = [tup for tup in inspect.getmembers(o1, lambda a: not (inspect.ismethod(a))) if not tup[0].startswith('_')]
        o2_vars = [tup for tup in inspect.getmembers(o1, lambda a: not (inspect.ismethod(a))) if not tup[0].startswith('_')]

        if len(o1_vars) != 0 and len(o2_vars) != 0:
            o1 = {member_name: member_value for member_name, member_value in o1_vars}
            o2 = {member_name: member_value for member_name, member_value in o2_vars}

    if o1 is not None and o2 is not None:
        # if both are dictionaries, we will compare each key
        if isinstance(o1, dict) and isinstance(o2, dict):
            if len(o1) != len(o2):
                return False
            for k in set().union(o1.keys(), o2.keys()):
                if k in o1 and k in o2:
                    if not deep_compare(o1[k], o2[k]):
                        return False
                else:
                    return False  # some key missing
            return True
        elif isinstance(o1, list) and isinstance(o2, list):
            if len(o1) != len(o2):
                return False
            for k1, k2 in zip(o1, o2):
                if not deep_compare(k1, k2):
                    return False
            return True
    try:
        return np.allclose(o1, o2, rtol=rtol, atol=atol)
    except TypeError:
        return o1 == o2
