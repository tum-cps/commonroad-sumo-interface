'''
Module tests of the module crsumo
'''
from sumo_interface_tests.common.marker import *

import docker

__author__ = "Peter Kocsis"
__copyright__ = "TUM Cyber-Physical System Group"
__credits__ = []
__version__ = "0.1"
__maintainer__ = "Peter Kocsis"
__email__ = "peter.kocsis@tum.de"
__status__ = "Integration"


@unit_test
@functional
def test_api_connection():
    """Tests whether it is allowed to call Docker without root privileges"""
    client = docker.from_env()
    if client is None or client.ping() is False:
        raise RuntimeError("Docker runtime is not responding!")