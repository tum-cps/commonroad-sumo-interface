'''
Module tests of the module crsumo
'''
from sumocr import DOCKER_REGISTRY
from sumocr.sumo_config import DefaultConfig
from sumocr.sumo_docker.interface.docker_interface import SumoInterface
from sumocr.sumo_docker.rpc.sumo_client import SumoRPCClient
from sumo_interface_tests.common.marker import *
import pytest
import docker.errors
import time

__author__ = "Peter Kocsis"
__copyright__ = "TUM Cyber-Physical System Group"
__credits__ = []
__version__ = "0.1"
__maintainer__ = "Peter Kocsis"
__email__ = "peter.kocsis@tum.de"
__status__ = "Integration"


@module_test
@functional
def test_rpc_connection():
    """Tests whether RPC connection can be established or not"""
    sumo_interface = SumoInterface()
    sumo_interface.start_simulator()
    sumo_interface.stop_simulator()


@module_test
@functional
def test_resetter():
    """Tests whether RPC connection can be established or not"""
    sumo_interface = SumoInterface()
    client = sumo_interface.start_simulator()
    client.conf = DefaultConfig
    client.conf.scenarios_path = "prev"
    assert client.conf.scenarios_path == "prev"
    client.reset_simulation()
    assert client.conf.scenarios_path != "prev", client.conf.scenarios_path
    sumo_interface.stop_simulator()


@pytest.mark.parametrize("grace_stop", [True, False])
@module_test
@functional
def test_auto_remove(grace_stop):
    """
    Tests whether the Docker container will be automatically removed after the shutdown
    :param grace_stop: Whether to stop the simulation gracefully or not
    """
    def start_stop() -> str:
        sumo_interface = SumoInterface()
        sumo_interface.start_simulator()
        container_id = sumo_interface._sumo_container.container.id
        if grace_stop:
            sumo_interface.stop_simulator()
        return container_id

    container_id = start_stop()
    time.sleep(1.0)
    client = docker.from_env()
    try:
        client.containers.get(container_id)
        raise RuntimeError("Auto remove unsuccessful")
    except docker.errors.NotFound:
        pass


@module_test
@functional
def test_exposed_port():
    sumo_interface = SumoInterface()
    sumo_interface.start_simulator()
    sumo_interface._sumo_container.prepare_docker(DOCKER_REGISTRY,
                                        "ver{}".format(SumoRPCClient.__version__),
                                        server_log_level='debug',
                                        docker_registry_address='gitlab.lrz.de:5005')
    sumo_interface._sumo_container.start()
    container_address_dict = sumo_interface._sumo_container.get_host_address()
    container_address_value = container_address_dict['50051/tcp']

