# 2022.3

- bugfix for duplicate edge ids (see issue #14)

# 2022.2

- ensure compatability with commonroad-io 2022.3

# 2022.1

- changed license to BSD 3-Clause (see LICENSE.txt)
- update requirements

# 2021.5

- improved speed of the SUMO co-simulation
- removed map conversion function that are now available in the commonroad scenario designer
